import QtQuick 2.9

Image {
    property string image: ""
    source: "qrc:/" + image
}
