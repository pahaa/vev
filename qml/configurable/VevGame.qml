import QtQuick 2.9
import QtQuick.Controls 2.2
import QtMultimedia 5.11
import modules.vev 1.0
import "." as Configured

Rectangle{
    id: root
    anchors.fill: parent

    Item{
        id: mainMenu
        anchors.fill: parent
        ImageItem {
            anchors.fill: parent
            image: "images/power.png"
        }
        Text {
            id: mainMenuLabel
            text: "Выберите, на чьей вы стороне"
            anchors{
                top: parent.top
                left: parent.left
                right: parent.right
                margins: 12
            }
            height: parent.height * 0.2
            font.pointSize: 50
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            wrapMode: Text.Wrap
        }
        Button{
            text: "Политические силы"
            font.pointSize: 50
            contentItem: Text {
                text: parent.text
                font: parent.font
                color: "red"
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                wrapMode: Text.Wrap
            }
            anchors{
                top: mainMenuLabel.bottom
                left: mainMenuLabel.left
            }
            height: parent.height * 0.5
            width: mainMenuLabel.width * 0.5 - 6
            onClicked: {
                player0.controlledByUser = true
                autoPlayer.target = location.power1
                mainMenu.visible = false
                hintPopup.visible = true
            }
            background: Rectangle {
                color: "grey"
                border.width: 6
                opacity: 0.3
            }
        }
        Button{
            text: "Политические силы"
            font.pointSize: 50
            contentItem: Text {
                text: parent.text
                font: parent.font
                color: "red"
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                wrapMode: Text.Wrap
            }
            anchors{
                top: mainMenuLabel.bottom
                right: mainMenuLabel.right
            }
            height: parent.height * 0.5
            width: mainMenuLabel.width * 0.5 - 6
            onClicked: {
                player1.controlledByUser = true
                autoPlayer.target = location.power0
                mainMenu.visible = false
                hintPopup.visible = true
            }
            background: Rectangle {
                color: "grey"
                border.width: 6
                opacity: 0.3
            }
        }

        //dev log
        Label{
            anchors.bottom: parent.bottom
            anchors.margins: 10
            anchors.left: parent.left
            anchors.right: logBtn.left
            wrapMode: Text.WrapAtWordBoundaryOrAnywhere
            horizontalAlignment: Text.AlignLeft
            text: "Пока идёт процесс разработки, я буду каждый день здесь описывать новые изменения, дабы поделиться своим энтузиазмом. Сервер ориентировочно будет включен каждый день с 10 утра до часу ночи.\n" +
                  "07.11 - разместил на хоккиапп бинарники для андройда"
            background: Rectangle{
                height: parent.height
                color: "white"
            }
        }
        Button{
            id: logBtn
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            anchors.margins: 10
            text: "История разработки"
            onClicked: logHistory.visible = true
        }
        Rectangle{
            id: logHistory
            anchors.fill: parent
            anchors.margins: 10
            visible: false
            ScrollView{
                anchors.fill: parent
                Text{
                    width: logHistory.width
                    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                    text: "24.10 - перевёл дев-версию на руби-сервер, теперь должно работать гораздо стабильнее; настроил универсальную сборку для мобильных и десктопных версий\n" +
                          "25.10 - принёс набор звуков (спасибо Славе), завтра буду подбирать; перевел тестовый билд на ad-hoc провижн; добавил временное окно с историей разработки\n" +
                          "27.10-5.11 - завал на работе\n" +
                          "07.11 - разместил на хоккиапп бинарники для андройда\n" +
                          ""
                }
            }
            Button{
                anchors.right: parent.right
                anchors.bottom: parent.bottom
                text: "Назад"
                onClicked: logHistory.visible = false
            }
        }
    }

    Item{
        id: hintPopup
        anchors.fill: parent
        visible: false
        Text{
            id: hintText
            width: 300
            anchors.centerIn: parent
            text: "Проводите политические акции для укрепления позиции в обществе.\n\n" +
                  "Привлекайте влиятельных лиц для увеличения финансирования, распределяемого на политические нужды.\n\n" +
                   "Чем выше ваша политическая сила, тем больше последователей захочет к вам присоединиться.\n\n" +
                   "Ваши последователи будут стоять за ваше дело до конца и попытаются сокрушить оппозиционные силы любыми доступными способами. " +
                   "К сожалению, как и в любом большом деле, в серьезной политической борьбе неизбежно будут происходить и случайные потери. " +
                   "Тем не менее, это не должно отвлекать вас от главной цели."

            verticalAlignment: Text.AlignVCenter
            //horizontalAlignment: Text.AlignHCenter
            wrapMode: Text.Wrap
        }
        Button{
            anchors{
                horizontalCenter: hintText.horizontalCenter
                top: hintText.bottom
                topMargin: 50
            }
            text: "Вперёд!"
            onClicked:{
                hintPopup.visible = false
                startGame()
            }
        }
    }

    Item{
        id: gameScreen
        anchors.fill: parent
        visible: false

        Configured.PlayerView{
            id: player0
            anchors.left: parent.left
            controlledGroup: location.power0
        }

        Configured.PlayerView{
            id: player1
            anchors.right: parent.right
            controlledGroup: location.power1
        }

        Configured.CountryView{
            id: countryView
            height: parent.height * 0.1
            anchors{
                left: parent.left
                right: parent.right
                bottom: parent.bottom
            }
            sourceGroup: location.neutrals
        }

        Rectangle{
            anchors{
                top: parent.top
                left: player0.right
                right: player1.left
                bottom: countryView.top
            }
            color: "yellow"
            Text{
                anchors.centerIn: parent
                text: "Карта Какого-то государства"
            }
        }

    }

    Item{
        id: loosePopup
        anchors.fill: parent
        visible: false
        ImageItem{
            id: loosePopupBg
            anchors.fill: parent
            image: "images/ruines.png"
            opacity: loosePopup.visible ? 1.0 : 0.0
            Text {
                anchors.centerIn: parent
                width: parent.width * 0.7
                color: "white"
                text: "Политическая сила победила: все граждане умерли.\nВозможно, в следующий раз вы предпочтёте жизни людей, а не политику?"
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                horizontalAlignment: Text.AlignHCenter
                font.pointSize: 40
            }
            OpacityAnimator on opacity {
                duration: 1000
            }
        }
        MouseArea{
            anchors.fill: parent
            onClicked: newGame()
        }
    }

    function newGame(){
        mainMenu.visible = true
        hintPopup.visible = false
        gameScreen.visible = false
        loosePopup.visible = false
        player0.controlledByUser = false
        player1.controlledByUser = false
        autoPlayer.target = null
        autoPlayer.frameCounter = 0
        locationSource.reload()
    }

    function startGame(){
        gameScreen.visible = true
        isGameRunning = true
    }

    function endGame(){
        gameScreen.visible = false
        loosePopup.visible = true
        isGameRunning = false
    }

    property bool isGameRunning: false
    property Timer mainTimer: Timer{
        repeat: true
        interval: 1000 * mainSettings.frameDuration
        running: isGameRunning
        triggeredOnStart: false
        onTriggered: {
            location.performFinancialActions()
            location.performAgitationActions()
            location.performFanaticActions()

            autoPlayer.perfromPlayerActions()
        }
    }

    property ReloadableInstance locationSource: ReloadableInstance{
        Component{
            SomeCountry{
                groups: [ neutralGroup, group0, group1 ]

                onExtinction: endGame()

                property int budget: 0

                property PoliticalGroup neutrals: PoliticalGroup{
                    id: neutralGroup
                    peopleGroups: [ workersGroup, oligarhGroup ]

                    property PeopleGroup workers: PeopleGroup{
                        id: workersGroup
                        people: mainSettings.startPopulation
                    }

                    property PeopleGroup oligarhs: PeopleGroup{
                        id: oligarhGroup
                        people: mainSettings.oligarhResidents
                    }
                }

                property PoliticalGroup power0: PoliticalPower{
                    id: group0
                    enemyGroup: group1
                    property var actionList: Configured.PoliticalActionList {}
                }

                property PoliticalGroup power1: PoliticalPower{
                    id: group1
                    enemyGroup: group0
                    property var actionList: Configured.PoliticalActionList {}
                }

                function performFinancialActions(){
                    budget += workersGroup.people * mainSettings.taxes
                    var groups = [ group0, group1 ]
                    for(var i=0; i < groups.length; ++i){
                        var group = groups[i]
                        var money = Math.min( group.oligarhs * mainSettings.bribe, budget )
                        budget -= money
                        group.budget += money
                    }
                }

                function performAgitationActions(){
                    var groups = [ group0, group1 ]
                    for(var i=0; i < groups.length; ++i){
                        var group = groups[i]
                        var recruts = Math.min( group.agitationPower, workersGroup.people )
                        workersGroup.people -= recruts
                        group.fans += recruts
                    }
                }
            }
        }
    }

    property SomeCountry location: locationSource.instance

    property AutoPlayer autoPlayer: AutoPlayer{ }

    function inviteOligarhs(count,group){
        var oligarhs = Math.min( count, location.neutrals.oligarhs.people )
        group.oligarhs += oligarhs
        location.neutrals.oligarhs.people -= oligarhs
    }

    property var mainSettings: Configured.MainSettings {}

    property Audio mainTheme: AudioItem{
        sourcePath: "sounds/theme.mp3"
        audioRole: Audio.GameRole
        autoPlay: true
        loops: Audio.Infinite
        muted: !Qt.application.active
    }

}
