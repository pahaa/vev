import QtQuick 2.9
import QtQuick.Controls 1.4
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.2
import modules.vev 1.0
import "configurable"

Window {
    id: rootWindow
    visible: true
    width: 1334
    height: 750
    title: "VEV"

    VevGame { }
    visibility: Window.FullScreen

}
