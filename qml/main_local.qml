import QtQuick 2.9
import QtQuick.Controls 1.4
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.2
import modules.vev 1.0

Window {
    id: rootWindow
    visible: true
    width: 1334
    height: 750
    title: "VEV"

    Loader {
        id: remoteRoot
        anchors.fill: parent
        property string host: "file:///" + resourceDir + "/configurable"
        active: false
        source: host + "/VevGame.qml"
    }

    Component.onCompleted:{
        remoteRoot.active = true
    }
}
