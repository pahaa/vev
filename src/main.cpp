#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QScreen>

#include "projectHelper.h"
#include "commonPaths.h"
#include "loadImageset.h"

#include "peoplegroup.h"
#include "politicalgroup.h"
#include "somecountry.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);
    FKUtility::setCurrentAppDirectory();
    FKUtility::replicateApplicationInfoFromProject();

    QSize screenSize(app.primaryScreen()->size());

    if(!FKUtility::loadImageset("images",screenSize)){
        qDebug("unable load imageset");
    }

    QQmlApplicationEngine engine;

    qmlRegisterType<SomeCountry>("modules.vev",1,0,"SomeCountry");
    qmlRegisterType<PoliticalGroup>("modules.vev",1,0,"PoliticalGroup");
    qmlRegisterType<PeopleGroup>("modules.vev",1,0,"PeopleGroup");

    engine.addImportPath("qrc:/");
    engine.rootContext()->setContextProperty("resourceDir",FKUtility::dataDir().absolutePath());

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}
