import qbs

Project {
    minimumQbsVersion: "1.12.0"

    property bool devBuild: true

    readonly property string CONTENT_EMBEDDED: "embedded"
    readonly property string CONTENT_LOCAL: "local"
    readonly property string CONTENT_REMOTE: "remote"

    property string contentConfiguration: {
        if (devBuild) {
            if (qbs.targetOS.contains("ios") || qbs.targetOS.contains("android")) {
                return CONTENT_REMOTE;
            } else {
                return CONTENT_LOCAL;
            }
        } else {
            return CONTENT_EMBEDDED;
        }
    }

    FKApplication {
        name: "VEV"
        maintainer: "Fajra Katviro"
        version: "1.0.1"

        Depends { name: "Android.ndk"; required: false }
        Android.ndk.appStl: "gnustl_shared"

        property pathList qmlImportPaths: ["qml", "qml/configurable"]

        skipValidation: false
        imagesets: project.contentConfiguration === project.CONTENT_EMBEDDED ? "art/imagesets" : undefined

        Group{
            name: "sources"
            files: "src/*"
        }

        Group{
            name: "qml(embedded)"
            condition: project.contentConfiguration === project.CONTENT_EMBEDDED
            prefix: "qml/"
            files: [
                "qml_main.qrc",
                "configurable.qrc",
                "modules.qrc"
            ]
        }

        Group{
            name: "qml(local)"
            condition: project.contentConfiguration === project.CONTENT_LOCAL
            prefix: "qml/"
            files: [
                "qml_main_local.qrc",
                "modules.qrc"
            ]
        }

        Group{
            name: "qml(local not compilable)"
            condition: project.contentConfiguration === project.CONTENT_LOCAL
            files: "qml/configurable/*.qml"
            fileTags: "fk.content"
            FK.content.sourceRoot: "qml"
        }

        Group{
            name: "art(local not compilable)"
            condition: project.contentConfiguration === project.CONTENT_LOCAL
            files: "art/imagesets/images/2048x1536/*.png"
            fileTags: "fk.content"
            FK.content.sourceRoot: "art/imagesets/images/2048x1536"
            FK.content.location: "constAppData/configurable/images"
        }

        Group{
            name: "qml(remote)"
            condition: project.contentConfiguration === project.CONTENT_REMOTE
            prefix: "qml/"
            files: [
                "qml_main_remote.qrc",
                "modules.qrc"
            ]
        }

        Group {
            name: "sounds"
            condition: project.contentConfiguration !== project.CONTENT_REMOTE
            files: "sounds/*"
            fileTags: "fk.content"
            FK.content.location: "constAppData/configurable"
        }

        Group {
            name: "qml sources"
            files: "qml/**/*.qml"
            condition: false
        }
    }

    Product{
        name: "gameInstaller"
        builtByDefault: false
        Depends{ name: "VEV" }
    }
}
